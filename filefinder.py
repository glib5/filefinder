from pathlib import Path


def get_files(path:Path=None, names:list[Path]=None) -> tuple[Path]:
    """
    recursively iterates through all the folders and files from the desktop
    onwards and returns all the files as a tuple of Path objects
    
    to be called without arguments
    """
    if path is None:
        path = Path(__file__).home() / "desktop"
    assert isinstance(path, Path)
    assert path.exists()
    if names is None:
        names = []
    assert isinstance(names, list)
    if path.is_file():
        names.append(path)
    elif path.is_dir():
        for p in path.iterdir():
            get_files(path=p, names=names)
    return tuple(names)


def find_file(filename:str) -> tuple[Path]:
    """
    finds a given file location. returns all files with the provided name
    
    Args:
        name (str): name of the file to be found. 
        - if the suffix is not provided, all files with this name
        are returned, otherwise the extension is used as an additional filter
        - case insensitive
    """
    assert isinstance(filename, str)
    needle = Path(filename.lower())
    if needle.suffix:
        filterfunc = lambda x: needle.name == x.name.lower()
    else:
        filterfunc = lambda x: needle.stem == x.stem.lower()
    return tuple(filter(filterfunc, get_files()))
